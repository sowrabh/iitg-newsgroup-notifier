function parse_post(element) {
	// console.log(element);
	var post = new Object();
	post.title = $(element).find("title").text();
	
	//post.link = $(element).find("guid").text();
	post.link = $.trim($(element).find('link').text());
	
	var temp=post.link.split("/");
	post.id=temp[temp.length-1];
	
	post.description = $(element).find('description').text();
	
    //post.creator = $(element).find("[nodeName=dc\\:creator]").text();
   post.creator = $(element).find('guid').prev().text();
   //console.log(post.creator);
	
	post.date=$(element).find('pubDate').text();
	post.category=$(element).find('category').text()
	var shorten = 120;
	if (post.title.length > 80) {
		shorten = 70;
	}
	
	post.description=format($(post.description).text());
	
	//console.log(post.description);
	return post;
}

function strip(html)
{
   var tmp = document.createElement("DIV");
   tmp.innerHTML = html;
   return tmp.textContent||tmp.innerText;
}

function format(text) {
	
	var shorten = 120;
	text = strip(text);
	text = $.trim(text);
	//alert("init "+ text);
	text =text.replace(/\s+/g,' ');
	text = text.substr(0, shorten);
	//alert("after" + text);
	return text;
}
function open_item(url) {
	chrome.tabs.create({url: url});
	//chrome.browserAction.setBadgeText({text:''});
}
